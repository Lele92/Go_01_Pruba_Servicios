package main

import (
	"log"
	"net/http"

	"gitlab.com/Lele92/01-Prueba_Servicios/people"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	people.SetPeopleHC()
	router.HandleFunc("/people", people.GetPeopleEndpoint).Methods("GET")
	router.HandleFunc("/people/{id}", people.GetPersonEndpoint).Methods("GET")
	router.HandleFunc("/people", people.CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people/{id}", people.DeletePersonEndpoint).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":12345", router))
}
