package people

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//Person estructura de datos para representar una persona
type Person struct {
	ID        int      `json:"id,omitempty"`
	FirstName string   `json:"firstname,omitempty"`
	LastName  string   `json:"lastname,omitempty"`
	Address   *Address `json:"address,omitempty"`
}

//Address estructura de datos para representar una dirección
type Address struct {
	City  string `json:"city,omitempty"`
	State string `json:"state,omitempty"`
}

//SetPeopleHC init
func SetPeopleHC() {
	connect()
}

//DeletePersonEndpoint servicio para eliminar una persona (DELETE a dominio:puerto/people/{ID})
func DeletePersonEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	id, _ := strconv.Atoi(params["id"])
	delete(id)
	json.NewEncoder(w).Encode(selectAll())
}

//CreatePersonEndpoint  servicio para crear una persona (PUT a dominio:puerto/people/{ID})
func CreatePersonEndpoint(w http.ResponseWriter, req *http.Request) {
	var person Person
	_ = json.NewDecoder(req.Body).Decode(&person)
	if person.ID == 0 {
		person = insert(person)
	} else {
		update(person)
	}
	json.NewEncoder(w).Encode(selectAll())
}

//GetPersonEndpoint servicio para visualizar datos de una persona (GET a dominio:puerto/people/{ID})
func GetPersonEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	id, _ := strconv.Atoi(params["id"])
	for _, item := range selectAll() {
		if item.ID == id {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Person{})
}

//GetPeopleEndpoint  servicio para visualizar datos todas las personas (GET a dominio:puerto/people)
func GetPeopleEndpoint(w http.ResponseWriter, req *http.Request) {
	json.NewEncoder(w).Encode(selectAll())
}
