package people

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error

func connect() {
	db, err = sql.Open("mysql", "tester:tester123@tcp(localhost:3306)/prueba01?charset=utf8")
	checkErr(err)
}

func disconnect() {
	db.Close()
}

func insert(person Person) Person {
	stmt, err := db.Prepare("INSERT person SET firstname=?, lastname=?, city=?, state=?")
	checkErr(err)
	if person.Address == nil {
		person.Address = new(Address)
	}
	res, err := stmt.Exec(person.FirstName, person.LastName, person.Address.City, person.Address.State)
	checkErr(err)
	id, err := res.LastInsertId()
	checkErr(err)
	person.ID = int(id)
	return person
}

func update(person Person) int64 {
	stmt, err := db.Prepare("UPDATE person SET firstname=?, lastname=?, city=?, state=? where id=?")
	checkErr(err)
	res, err := stmt.Exec(person.FirstName, person.LastName, person.Address.City, person.Address.State, person.ID)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	return affect
}

func selectAll() []Person {
	rows, err := db.Query("SELECT * FROM person")
	checkErr(err)
	var people []Person
	for rows.Next() {
		var person Person
		person.Address = new(Address)
		err = rows.Scan(&person.ID, &person.FirstName, &person.LastName, &person.Address.City, &person.Address.State)
		people = append(people, person)
	}
	return people
}

func delete(id int) int64 {
	stmt, err := db.Prepare("DELETE FROM person where id=?")
	checkErr(err)
	res, err := stmt.Exec(id)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	return affect
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
